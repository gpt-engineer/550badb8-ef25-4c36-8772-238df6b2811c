// Get the current year
const currentYear = new Date().getFullYear();
// Set the current year in the footer
document.getElementById("currentYear").textContent = currentYear;

// Get the modal
var modal = document.getElementById("myModal");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the page loads, display the modal
window.onload = function () {
  modal.style.display = "block";
};

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
  modal.style.display = "none";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
};

// Slot game
const slotMachine = {
  slots: ["🍒", "🍋", "🍊", "🍇", "🍉"],
  spin() {
    const index = Math.floor(Math.random() * this.slots.length);
    return this.slots[index];
  },
};

// Spin button
const spinButton = document.getElementById("spin-button");
spinButton.addEventListener("click", () => {
  const slots = Array.from({ length: 3 }, (_, i) =>
    document.getElementById(`slot${i + 1}`),
  );
  const result = document.getElementById("slot-result");

  // Check if slots and result are selected correctly
  if (!slots.every(Boolean) || !result) {
    console.error("Cannot select slots or result");
    return;
  }

  if (Math.random() <= 0.15) {
    // 50% chance to win
    const winningSymbol = slotMachine.spin();
    slots.forEach((slot) => {
      slot.textContent = winningSymbol;
    });
    result.textContent = "You win!";
    showWinPopup();
  } else {
    // 50% chance to lose
    slots.forEach((slot) => {
      let symbol;
      do {
        symbol = slotMachine.spin();
      } while (symbol === slots[0].textContent);
      slot.textContent = symbol;
    });
    result.textContent = "Try again!";
  }
});

// Function to show a popup when the user wins
function showWinPopup() {
  const winPopup = document.getElementById("winPopup");
  const closeWinPopupButton = document.getElementById("closeWinPopup");

  // Check if winPopup and closeWinPopupButton are selected correctly
  if (!winPopup || !closeWinPopupButton) {
    console.error("Cannot select winPopup or closeWinPopupButton");
    return;
  }

  // Display the win popup
  winPopup.style.display = "block";

  // Hide the win popup when the close button is clicked
  closeWinPopupButton.addEventListener("click", () => {
    winPopup.style.display = "none";
  });
}

// New RPG game
const playRpgButton = document.getElementById("play-rpg");
playRpgButton.addEventListener("click", () => {
  // Payment process
  const payment = confirm(
    "This game requires a payment of $10. Do you want to proceed?",
  );
  if (payment) {
    // Game logic
    startRpgGame();
  } else {
    alert("Payment cancelled. You can't play the game.");
  }
});

// RPG game logic
function startRpgGame() {
  alert("Welcome to the new RPG game. Let your desires guide you.");
  const playerName = prompt("Enter your character's name:");
  const playerClass = prompt("Choose your class: Warrior, Mage, or Thief");
  alert(
    `Welcome ${playerName}, the ${playerClass}. Your adventure begins now.`,
  );
  const firstChoice = prompt(
    "You see a cave and a forest. Where do you go? (cave/forest)",
  );
  if (firstChoice === "cave") {
    alert("You enter the cave and find a treasure chest. You win!");
  } else if (firstChoice === "forest") {
    alert("You enter the forest and are attacked by a bear. You lose!");
  } else {
    alert("Invalid choice. Game over.");
  }
}
